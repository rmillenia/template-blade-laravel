@extends('adminlte.master')

@section('contentheader')
<section class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1>Edit Pertanyaan {{$pertanyaan->id}}</h1>
			</div>
		</div>
	</div>
</section>
@endsection

@section('content')
<div class="card">
	<div class="card-body">
		<form action="/pertanyaan/{{$pertanyaan->id}}" method="POST">
			@csrf
			@method('PUT')
			<div class="form-group">
				<label for="judul">Judul</label>
				<input type="text" class="form-control" name="judul" value="{{$pertanyaan->judul}}" id="judul" placeholder="Masukkan Judul">
				@error('judul')
				<div class="alert alert-danger">
					{{ $message }}
				</div>
				@enderror
			</div>
			<div class="form-group">
				<label for="isi">Isi</label>
				<input type="text" class="form-control" name="isi"  value="{{$pertanyaan->isi}}"  id="isi" placeholder="Masukkan Isi">
				@error('isi')
				<div class="alert alert-danger">
					{{ $message }}
				</div>
				@enderror
			</div>
			<button type="submit" class="btn btn-primary">Edit</button>
		</form>
	</div>
</div>
@endsection