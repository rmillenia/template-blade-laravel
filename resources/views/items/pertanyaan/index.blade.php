@extends('adminlte.master')

@section('contentheader')
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>List Pertanyaan</h1>
    </div>
</div>
</div>
</section>
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <a href="/pertanyaan/create" class="btn btn-primary">Tambah</a>
        <br><br>
        <table class="table">
            <thead class="thead-light">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Judul</th>
                    <th scope="col">Isi</th>
                    <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($pertanyaan as $key=>$value)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$value->judul}}</td>
                    <td>{{$value->isi}}</td>
                    <td>
                        <a href="/pertanyaan/{{$value->id}}" class="btn btn-info">Show</a>
                        <a href="/pertanyaan/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                        <form action="/pertanyaan/{{$value->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-danger my-1" value="Delete">
                        </form>
                    </td>
                </tr>
                @empty
                <tr colspan="3">
                    <td>No data</td>
                </tr>  
                @endforelse              
            </tbody>
        </table>
    </div>
</div>
@endsection