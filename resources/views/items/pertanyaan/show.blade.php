@extends('adminlte.master')

@section('contentheader')
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Lihat Pertanyaan {{$pertanyaan->id}}</h1>
            </div>
        </div>
    </div>
</section>
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <h4>{{$pertanyaan->judul}}</h4>
        <p>{{$pertanyaan->isi}}</p>
        <br>
        <a href="/pertanyaan" class="btn btn-info">Back</a>
    </div>
</div>
@endsection